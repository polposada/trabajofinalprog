'use strict'

const mongoose = require('mongoose')
const app = require('./app')
const config = require('./config')

// const port = process.env.PORT || 3000

// Metodo de conexion entre el mongose, archivos y localhost
mongoose.connect(config.db, (err, res) => {
    if (err){
        return console.log(`Error al conectar a la base de datos ${err}`)
    }

    console.log('Conexion a la base de datos establecida...')
    
    app.listen(config.port, ()=>{
        console.log(`API Rest corriendo en http://localhost:${config.port}`);
    })

})