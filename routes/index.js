'use strict'

const express = require('express')
const rutaCtrl = require('../controller/ruta')
const userCtrl = require('../controller/user')
const auth = require('../middlewares/auth')
const api = express.Router()

// Trae todas las rutas
api.get('/ruta', rutaCtrl.getRutas)
// Trae todas una ruta
api.get('/ruta/:rutaId',  rutaCtrl.getRuta)
// Metodo post para agregar la ruta en la app (No NECESARIA)
api.post('/ruta', rutaCtrl.saveRuta)
// Metodo put para actualizar la informacion de la ruta (NO NECESARIA)
api.put('/ruta/:rutaId', rutaCtrl.updateRuta)
//Metodo Delete
api.delete('/ruta/:rutaId', rutaCtrl.deleteRuta)
//Metodos para logear
api.post('/signup', userCtrl.signUp)
api.post('/signin', userCtrl.signIn)
//Metodo para autenticar
api.get('/private',auth ,function(req, res){
    res.status(200).send({ message: 'Tienes Acceso'})
})

module.exports = api