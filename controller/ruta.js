'use strict'

const Ruta = require('../models/ruta')

//GET - Returna todos los in the DB
function getRutas(req, res) {
    Ruta.find({}, (err, ruta) => {
        if (err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
        if (!ruta) return res.status(404).send({message: `La ruta existe`})
    
        res.status(200).send({ruta})
    })
};

//GET - Returna una ruta con ID especifica
function getRuta(req, res) {
	let rutaId = req.params.rutaId

    Ruta.findById(rutaId, (err, ruta) => {
        if (err) return res.status(500).send({message: `Error al realizar la peticion: ${err}`})
        if (!ruta) return res.status(404).send({message: `La ruta existe`})
    
        res.status(200).send({ruta})
    })
};

//POST - Insertar una nueva ruta en la DB
function saveRuta(req, res) {
    console.log('POST /test/ruta')
    console.log(req.body)

    let ruta = new Ruta()
    ruta.id = req.body.id


    ruta.save((err, rutaStored) => {
        if (err) res.status(500).send({message: `Error al guardar la BASE de DATOS: ${err}`})

        res.status(200).send({rutaStored})
    })
};

//PUT - Actualizar la ruta
function updateRuta(req, res) {
    let rutaId = req.params.rutaId
    let update = req.body
    
    Ruta.findByIdAndUpdate(rutaId, update, (err, rutaUpdated) => {
        if (err) return res.status(500).send({message: `Error al ACTUALIZAR la ruta: ${err}`})
        
        res.status(200).send({ruta: rutaUpdated})        
    })
};

//DELETE - Delete a ruta especifica
function deleteRuta(req, res) {
    let rutaId = req.params.rutaId

    Ruta.findById(rutaId, (err, ruta) => {
        if (err) return res.status(500).send({message: `Error al BORRAR la ruta: ${err}`})
        
        ruta.remove(err => {
            if (err) return res.status(500).send({message: `Error al BORRAR la ruta: ${err}`})
            res.status(200).send({message: 'La ruta ha sido ELIMINADA'})
        })
    })
};

module.exports = {
	getRuta,
	getRutas,
	saveRuta,
	updateRuta,
	deleteRuta
}