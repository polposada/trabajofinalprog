'use strict'
 
const User = require('../models/user')
const service = require('../services')

function signUp (req, res){
    const user = new User({
        email: req.body.email,
        displayName: req.body.displayName,
        password: req.body.password
    })

    user.avatar = user.gravatar();

    user.save((err)=> {
        if (err) res.status(500).send({message: `Error al crear el usuario ${err}`})
    })
}

function signIn (req, res){
  User.findOne({ email: req.body.email }, (err, user) => {
    if (err) return res.status(500).send({ message: 'Error al ingresar: ${err}'})
    if (!user) return res.status(404).send({ message: 'No existe el usuario: $(req.body.email)' })

    return user.comparePassword(req.body.password, (err, isMatch) => {
        req.user = user
        res.status(200).send({
        message: 'Te has logueado correctamente',
        token: service.createToken(user)
        })
    });
}).select('_id email +password');
}

module.exports ={
    signIn,
    signUp
}