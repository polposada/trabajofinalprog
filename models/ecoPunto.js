'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PuntoSchema = Schema({
    id: String,
    name: String,
    descripcion: String,
    imagen: String,
})

module.exports = mongoose.model('Punto', PuntoSchema)