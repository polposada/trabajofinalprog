'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const RutaSchema = Schema({
    id: String
})

module.exports = mongoose.model('Ruta', RutaSchema)
